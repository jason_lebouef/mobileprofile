# Set a system up with default settings
# Source functions I will need
source ~/bin/prefs/vimOptions.cfg
source ~/bin/prefs/gitOptions.cfg

hostname=`hostname -s`
echo "Hello ${GREEN}${hostname},${NORMAL} setting up your environment"
echo ""
mkdir ~/bin/hosts                > /dev/null 2>&1
echo "Defining Colors for the terminal..."
echo "# Color Options"           > ~/bin/hosts/${hostname}.host
cat ~/bin/prefs/colorOptions.cfg >> ~/bin/hosts/${hostname}.host
echo "Define Personal Variables"
# Building variables

# VIM Setup
export LOOP=0
echo "Will you need VIM defined?"
while [ $LOOP -eq 0 ]; 
  do
    read yn; 
    case $yn in
      "yes") # Declare VIM variables
             vimSetup 
             export LOOP=1
             ;;

      "no" ) # Declare VIM CHK Varialable to skip install later
             export LOOP=1
             ;;

      *    ) # Only accept yes or no
             export LOOP=0
             echo "Please enter yes or no"
             ;;
  esac
done

# Git setup - git user information and alias
gitSetup

# Send this configutation file to .gitconfig
echo "# Creating .gitconfig file..."
cp ~/bin/hosts/gitConf.cfg  ~/.gitconfig

# Add additional functions
echo "# Git Functions"      >> ~/bin/hosts/${hostname}.host
cat ~/bin/prefs/gitFunc.cfg >> ~/bin/hosts/${hostname}.host
source ~/bin/hosts/${hostname}.host
