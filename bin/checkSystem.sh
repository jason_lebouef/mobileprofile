hostname=${HOSTNAME}

# Check if hostname is already set.  If not, set source and variables

if [[ -f ~/bin/hosts/$(hostname -s).host ]]; 
  then
    hostname=`hostname -s`
    source ~/bin/hosts/$hostname.host
    colorOptions
  else
    source ~/bin/setSystem.sh
fi
