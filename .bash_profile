export PATH="$HOME/bin:$PATH:/Applications/VMware Fusion.app/Contents/Library/"

# In order to maintain the called functions, I need to source checkSystems.sh
source checkSystem.sh

# Call Color Function
colorOptions

# Check out local git repo
gitCheck
#checkJavaPlugin
#diffModule

export PS1="[\[${BOLD}${MAGENTA}\]\u\[$WHITE\]@\[$WHITE\]\h\[$WHITE\] \[$GREEN\]\w\[$WHITE\]\$(is_on_git && [[ -n \$(git branch 2> /dev/null) ]] && echo \":\")\[$PURPLE\]\$(parse_git_branch)\[$WHITE\]\[$ORANGE\]\[$WHITE\]]# \[$RESET\]"
