# **This is my default Profile repo** #
This creates an initial bash environment with my git and vim preferences.  Ideally this would request a puppet repo similiar to Docker.

### What this will do ###
 - bash_profile - This will be used as the primary source for changes
 - checkSystem.sh - This runs from bash_profile; It checks for a file based off of the hostnmame in ~/bin/hosts/
                    If one does exist, if sources existing functions and variables
                    If one does not exists, it continues through the system setup (setSystem.sh)
 - setSystem.sh - Initially, this will call vimOptions and gitOptions to use existing functions that will create the host profile
                - Creates initial evironment
                -- ~/bin/hosts (Not tracked through git)
                --- colorOptions.cfg  - Color options for system
                -- vimOptions.cfg     - This is called from function _vimSetup_ to create a VIM directory if you want one (may be a bit buggy)
                -- gitSetup           - Calls the following configurations
                --- gitAliases.cfg    - All of my currect gitAliases
                --- gitFunc.cfg       - Cool gitFunctions to help with git repos
                --- gitOptions.cfg    - This builds a .gitconfig and will overwrite the current one
 - All of this is sent to a ~/bin/hosts/{hostfile}.host which is then sourced to create environment
               

### Instructions to use ###
1.  Clone to home directory
2.  source .bash_profile
3.  Run through the command prompts

This only includes the following directories:

- .gitignore    -> To provide the essential list of items needed

- .bash_profile -> To provide default Bash Environment

- bin/          -> Runs scripts to source out system variable changes to a server config folder that is untracked - Contains structure

- prefs/        -> Listing of functions and variables to set as default

Note: I am only ending the scripts with .sh because I like vim plugins to help me with syntax and pretty colors
### Things to try ###
- Develop additonal system check items and a clear list?  

- Have username root submit as sudo username's git variable's
For example:
If puppet is not installed, let you know and then change a file to confirm if it should be loaded. 

 - checkout -> http://serverfault.com/questions/256754/correct-user-names-when-tracking-etc-in-git-repository-and-committing-as-root
 - Testing:  Did not work on test machine

- How to make this faster!!!
- How to push to JSON or XML for Host... have not decide which is better
- Push VIM configs to traveler maybe or now to move on to pushing from puppet
- Is it possible to make this work somehow on all environments? (Windows)